# Interfaces para scripts em shell

## Índice

### [Aula 1 - Introdução](aula-01-introducao.md)

[1.1 - O que é uma interface](aula-01-introducao.md#1-1-o-que-%C3%A9-uma-interface)
[1.2 - O shell como interface](aula-01-introducao.md#1-2-o-shell-como-interface)

- [Fluxos de dados padrão](aula-01-introducao.md#fluxos-de-dados-padr%C3%A3o)
- [Dispositivos são interfaces](aula-01-introducao.md#dispositivos-s%C3%A3o-interfaces)
- [Processos como interfaces](aula-01-introducao.md#processos-como-interfaces)
- [E o meu script com isso?](aula-01-introducao.md#e-o-meu-script-com-isso)

[1.3 - Classes de interfaces para scripts](aula-01-introducao.md#1-3-classes-de-interfaces-para-scripts)

- [Interface para a linha de comando (CLI)](aula-01-introducao.md#interface-para-a-linha-de-comando-cli)
- [Interface com o usuário via terminal (TUI)](aula-01-introducao.md#interface-com-o-usu%C3%A1rio-via-terminal-tui)
- [Interface gráfica com o usuário(GUI)](aula-01-introducao.md#interface-gr%C3%A1fica-com-o-usu%C3%A1rio-gui)

## Parte 1 - Interfaces CLI

### [Aula 2 - Recebendo dados pela linha de comando](aula-02-recebendo-argumentos.md)

[2.1 - Parâmetros especiais](aula-02-recebendo-argumentos.md#2-1-par%C3%A2metros-especiais)

- [Variáveis ou parâmetros?](aula-02-recebendo-argumentos.md#vari%C3%A1veis-ou-par%C3%A2metros)

[2.2 - Parâmetros posicionais](aula-02-recebendo-argumentos.md#2-2-par%C3%A2metros-posicionais)

- [O conceito de 'palavra'](aula-02-recebendo-argumentos.md#o-conceito-de-palavra)
- [Regras de citação](aula-02-recebendo-argumentos.md#regras-de-cita%C3%A7%C3%A3o)

[2.3 - Expandindo parâmetros](aula-02-recebendo-argumentos.md#2-3-expandindo-par%C3%A2metros)

- [O comando 'set'](aula-02-recebendo-argumentos.md#o-comando-set)
- [Expandindo argumentos](aula-02-recebendo-argumentos.md#expandindo-argumentos)
- [Expandindo parâmetros com '@' e '*'](aula-02-recebendo-argumentos.md#expandindo-par%C3%A2metros-com-e)

[2.4 - Expandindo argumentos em 'loops'](aula-02-recebendo-argumentos.md#2-4-expandindo-argumentos-em-loops)

- [O loop 'for' no estilo C](aula-02-recebendo-argumentos.md#o-loop-for-no-estilo-c)
- [Expandindo parâmetros com os loops 'while' e 'until'](aula-02-recebendo-argumentos.md#expandindo-par%C3%A2metros-com-os-loops-while-e-until)

[2.5 - Parâmetros em pilhas de execução do Bash](aula-02-recebendo-argumentos.md#2-5-par%C3%A2metros-em-pilhas-de-execu%C3%A7%C3%A3o-do-bash)

- [Os vetores 'BASH_ARGC' e 'BASH_ARGV'](aula-02-recebendo-argumentos.md#os-vetores-bash_argc-e-bash_argv)
- [A opção 'extdebug'](aula-02-recebendo-argumentos.md#a-op%C3%A7%C3%A3o-extdebug)
- [Quando 'BASH_ARGC' e 'BASH_ARGV' recebem valores](aula-02-recebendo-argumentos.md#quando-bash_argc-e-bash_argv-recebem-valores)
- [Habilitando 'extdebug' no corpo da função](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-02-recebendo-argumentos.md#habilitando-extdebug-no-corpo-da-fun%C3%A7%C3%A3o)

### [Aula 3 - Argumentos não-ordenados](aula-03-argumentos-nao-ordenados.md)

[3.1 - Associação de argumentos a padrões de texto](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#3-1-associa%C3%A7%C3%A3o-de-argumentos-a-padr%C3%B5es-de-texto)

[3.2 - Argumentos nomeados](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#3-2-argumentos-nomeados)

- [Tipos de argumentos nomeados](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#tipos-de-argumentos-nomeados)
  - [Identificação de valores](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#identifica%C3%A7%C3%A3o-de-valores)
  - [Comandos](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#comandos)
  - [Opções (ou 'flags')](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#op%C3%A7%C3%B5es-ou-flags)
  - [Uma reflexão sobre o termo 'flag'](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#uma-reflex%C3%A3o-sobre-o-termo-flag)

[3.3 - Iteração por parâmetros](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#3-3-itera%C3%A7%C3%A3o-de-par%C3%A2metros)

- [Toda regra tem uma exceção](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#toda-regra-tem-uma-exce%C3%A7%C3%A3o)
- [Deslocamento de parâmetros ('shift')](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#deslocamento-de-par%C3%A2metros-shift)
- [Padronização e tratamento de erros](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#padroniza%C3%A7%C3%A3o-e-tratamento-de-erros)
- [Convenções são definições de padrões](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#conven%C3%A7%C3%B5es-s%C3%A3o-defini%C3%A7%C3%B5es-de-padr%C3%B5es)

[3.4 - O comando interno 'getopts'](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#3-4-o-comando-interno-getopts)

- [Argumentos inesperados](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#argumentos-inesperados)
- [Variáveis 'OPTIND', 'OPTARG' e 'OPTERR'](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#vari%C3%A1veis-optind-optarg-e-opterr)
- [Argumentos obrigatórios](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#argumentos-obrigat%C3%B3rios)

[3.5 - Argumentos mistos](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#3-5-argumentos-mistos)

- [Separador de argumentos](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#separador-de-argumentos)
- [Capturando parâmetros depois do separador](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/aula-03-argumentos-nao-ordenados.md#capturando-par%C3%A2metros-depois-do-separador)

### [Aula 4 - Recebendo dados por 'pipes'](#)

#!/usr/bin/env bash

# Uma função que receberá argumentos...

func() {
    for i in "${!BASH_ARGC[@]}"; do
        printf 'Pilha %s → Qtd: %s\n' $i ${BASH_ARGC[i]}
    done
    for i in "${!BASH_ARGV[@]}"; do
        printf 'Índice %s → %s\n' $i "${BASH_ARGV[i]}"
    done
}

printf 'PARÂMETROS DO FLUXO PRINCIPAL...\n\n'

for i in "${!BASH_ARGC[@]}"; do
    printf 'Pilha %s → Qtd: %s\n' $i ${BASH_ARGC[i]}
done
for i in "${!BASH_ARGV[@]}"; do
    printf 'Índice %s → %s\n' $i "${BASH_ARGV[i]}"
done

printf '\nPARÂMETROS DA SUB-ROTINA...\n\n'

func x y z

printf '\nPARÂMETROS DO FLUXO PRINCIPAL (DEPOIS DA FUNÇÃO)...\n\n'

for i in "${!BASH_ARGC[@]}"; do
    printf 'Pilha %s → Qtd: %s\n' $i ${BASH_ARGC[i]}
done
for i in "${!BASH_ARGV[@]}"; do
    printf 'Índice %s → %s\n' $i "${BASH_ARGV[i]}"
done

#!/usr/bin/env bash

echo "Parâmetros em '@': $@"
echo "Valor inicial em 'OPTIND': $OPTIND"

while getopts ':ab' arg; do
    echo ---
    echo "Valor em 'arg': $arg"
    echo "Valor em 'OPTIND': $OPTIND"
    echo "Valor em 'OPTARG': $OPTARG"
done

echo ---

echo "Parâmetros em '@': $@"

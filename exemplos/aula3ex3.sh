#!/usr/bin/env bash

ajuda() {
    echo 'Isso é uma ajuda. Bye!'
    exit
}

versao() {
    echo 'Esta é a versão. Fui!'
    exit
}

comando() {
    echo 'Pronto! Sempre às ordens!'
    exit
}

case $1 in
    -h|--help)    ajuda;;
    -v|--version) versao;;
    -c|--command) comando;;
esac

echo 'Já que você não quer nada... T+!'

#!/usr/bin/env bash

echo "\$@ antes do processamento: $@"

# A expansão "$@" servirá de referência para o fim
# do loop 'while' quando não expandir mais nada...
while [[ "$@" ]]; do
    # Agora, só precisamos da expansão '$1'
    case "$1" in
        -a) # Se o padrão casar com '-a', o valor em questão
            # será o segundo parâmetro posicional disponível...
            var1="$2"
            ;;
        -b) # Se o padrão casar com '-b', o valor em questão
            # será o segundo parâmetro posicional disponível...
            var2="$2"
            ;;
    esac
    # O primeiro parâmetro posicional é removido...
    shift
done

echo "var1=$var1"
echo "var2=$var2"
echo "\$@ depois do processamento: ${@:-nada}"

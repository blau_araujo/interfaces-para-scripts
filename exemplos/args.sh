#!/usr/bin/env bash

for i in "${!BASH_ARGC[@]}"; do
    printf 'Pilha %s → Qtd: %s\n' $i ${BASH_ARGC[i]}
done

for i in "${!BASH_ARGV[@]}"; do
    printf 'Índice %s → %s\n' $i "${BASH_ARGV[i]}"
done

#!/usr/bin/env bash

echo "\$@ antes do processamento: $@"

# Percorrer todos os parâmetros recebidos...
for arg in "$@"; do

    # Somente as expansões de 'arg' que casarem com os padrões
    # nas cláusulas do 'case' resultarão em alguma ação...
    case $arg in

        -a) # Se o padrão casar com '-a', o valor em questão
            # será o segundo parâmetro em '@'...
            var1="${@:2:1}"
            ;;

        -b) # Se o padrão casar com '-b', o valor em questão
            # também será o segundo parâmetro '@'...
            var2="${@:2:1}"
            ;;
    esac

    # O primeiro parâmetro em '@' é removido...
    shift
done

echo "var1=$var1"
echo "var2=$var2"
echo "\$@ depois do processamento: ${@:-nada}"

#!/usr/bin/env bash

# Descomente para ver a diferença que faz sobre os vetores...
# printf "Nós temos ${BASH_ARGC[@]} argumentos.\n\n"

# Uma função que receberá argumentos...

func() {
    # Consultando o estado de 'BASH_ARGC'...
    declare -p BASH_ARGC
}

printf 'CHAMADA SEM ARGUMENTOS...\n\n'

func

printf '\nCHAMADA COM ARGUMENTOS...\n\n'

func x y z

printf '\nNOVA CHAMADA SEM ARGUMENTOS...\n\n'

func

#!/usr/bin/env bash

# Habilitando 'extdebug'...
shopt -s extdebug

# Uma função que receberá argumentos...

func() {
    for i in "${!BASH_ARGC[@]}"; do
        printf 'Pilha %s → Qtd: %s\n' $i ${BASH_ARGC[i]}
    done
    for i in "${!BASH_ARGV[@]}"; do
        printf 'Índice %s → %s\n' $i "${BASH_ARGV[i]}"
    done
}

printf 'CHAMADA SEM ARGUMENTOS...\n\n'

func

printf '\nCHAMADA COM ARGUMENTOS...\n\n'

func x y z

printf '\nNOVA CHAMADA SEM ARGUMENTOS...\n\n'

func

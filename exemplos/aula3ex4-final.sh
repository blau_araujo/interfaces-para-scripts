#!/usr/bin/env bash

echo "\$@ antes do processamento: $@"

# A expansão "$@" servirá apenas para termos a quantidade
# necessária de iterações pelos parâmetros...
for arg in "$@"; do

    # A cada iteração, o 'shift' será executado fazendo
    # com que a expansão '$1' seja sempre igual à
    # expansão de 'arg'...
    case $arg in

        -a) # Se o padrão casar com '-a', o valor em questão
            # será o segundo parâmetro posicional disponível...
            var1="$2"
            ;;

        -b) # Se o padrão casar com '-b', o valor em questão
            # será o segundo parâmetro posicional disponível...
            var2="$2"
            ;;
    esac

    # O primeiro parâmetro em '@' é removido...
    shift
done

echo "var1=$var1"
echo "var2=$var2"
echo "\$@ depois do processamento: ${@:-nada}"

#!/usr/bin/env bash

# Descomente para ver a diferença que faz sobre os vetores...
# printf "Nós temos ${BASH_ARGC[@]} argumentos.\n\n"

# Uma função que receberá argumentos...

func() {
    shopt -s extdebug

    echo ARGC "${BASH_ARGC[@]}"
    echo ARGV "${BASH_ARGV[@]}"
}

printf 'CHAMADA SEM ARGUMENTOS...\n\n'

func

printf '\nCHAMADA COM ARGUMENTOS...\n\n'

func x y z

printf '\nNOVA CHAMADA SEM ARGUMENTOS...\n\n'

func

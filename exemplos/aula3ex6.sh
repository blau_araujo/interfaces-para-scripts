#!/usr/bin/env bash

echo "Parâmetros em '@': $@"
echo "Valor inicial em 'OPTIND': $OPTIND"

while getopts ':ab' arg; do
    echo ---
    echo "Valor em 'arg': $arg"
    echo "Valor em 'OPTIND': $OPTIND"
    echo "Valor em 'OPTARG': $OPTARG"
done

echo ---

echo "Valor em 'OPTIND': $OPTIND"

echo ---

echo "Parâmetros em '@' antes do shift: $@"

shift $((OPTIND-1))

echo "Valor de 'N' (OPTIND-1): $((OPTIND-1))"
echo "Parâmetros em '@' depois do shift: $@"

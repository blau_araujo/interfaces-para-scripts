#!/usr/bin/env bash

uso='Uso: aula3ex2.sh [n=NOME] [s=SOBRENOME]'

(($#)) || { echo "$uso"; exit 1; }

for arg in "$@"; do
    case ${arg%%=*} in
        n) n="${arg#*=}" ;;
        s) s="${arg#*=}" ;;
        *) echo "$uso"; exit 1
    esac
done

[[ "$s" ]] && s="$s." || n="$n."
[[ "$n" ]] && n="$n " || n=''

echo "Seu nome é $n$s"
